import { ContainerStyled } from "./styles";
import Menu from "../../Components/menu";
import CardOverview from "../../Components/cardOverview";
import SalesReport from "../../Components/salesReport";
import TaskReport from "../../Components/tasks";
import Chart from "../../Components/chart";

import { useTestData } from "../../provider/testProvider";

const OverviewPage = () => {
  const { testData } = useTestData();
  return (
    <ContainerStyled>
      <Menu title={"Visão Geral"} />
      <section className="FeaturedInfoStyled">
        <div className="Card-total">
          <h3>Total de Lojas</h3>
          <div>{testData[0].totalDeLojas}</div>
        </div>
        <CardOverview
          title={"Faturamento total"}
          content={testData[0].faturamentoTotal}
        />
        <CardOverview
          title={"Loja destaque"}
          content={testData[0].lojaDestaque}
        />
        <CardOverview title={"Meta Mensal"} content={testData[0].metaMensal} />
      </section>
      <section className="Box-chart">
        <Chart />
      </section>
      <section className="Box-tickets-tasks">
        <div className="Box-tickets">
          <div className="Content">
            <SalesReport />
          </div>
        </div>
        <div className="Box-tasks">
          <div className="Content">
            <TaskReport />
          </div>
        </div>
      </section>
    </ContainerStyled>
  );
};
export default OverviewPage;
