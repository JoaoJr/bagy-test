import RoutesPages from "./Routes";
import SideBar from "./Components/sideBar";

function App() {
  return (
    <div className="App">
      <SideBar />
      <RoutesPages />
    </div>
  );
}

export default App;
