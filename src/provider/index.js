// import DataProvider from "./dataProvider";
import { TestProvider } from "./testProvider";
import { FiltersProvider } from "./filtersProvider";

const Providers = ({ children }) => {
  return (
    <TestProvider>
      <FiltersProvider>{children}</FiltersProvider>
    </TestProvider>
  );
};

export default Providers;
