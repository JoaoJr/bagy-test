import { createContext, useContext, useState } from "react";
import { data2 } from "../../Mock";

export const TestContext = createContext();

export const TestProvider = ({ children }) => {
  const [testData, setTestData] = useState(data2);

  return (
    <TestContext.Provider value={{ testData, setTestData }}>
      {children}
    </TestContext.Provider>
  );
};

export const useTestData = () => useContext(TestContext);
