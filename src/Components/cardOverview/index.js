import { CardStyled } from "./styles";

const CardOverview = ({ title, content }) => {
  return (
    <>
      <CardStyled>
        <h3>{title}</h3>
        <div>{content}</div>
      </CardStyled>
    </>
  );
};

export default CardOverview;
