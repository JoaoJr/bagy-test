import styled from "styled-components";

export const ContainerStyled = styled.div`
  display: flex;

  .Box-chart-info {
    width: 780px;
    padding: 15px;
    display: flex;
    flex-direction: column;
    align-items: stretch;
    justify-content: space-evenly;

    .chart-info-top {
      display: flex;
      justify-content: space-between;
      height: 100px;
      margin-bottom: 20px;

      .title {
        width: 40%;
        h3 {
          font-size: 18px;
          font-weight: bold;
          margin-bottom: 5px;
        }
        span {
          color: var(--gray);
          font-size: 14px;
          font-weight: bold;
          text-transform: uppercase;
        }
      }
      .legend {
        display: flex;
        flex-direction: row-reverse;
        width: 60%;

        .strokes {
          width: 18px;
          height: 4px;
          margin: 10px;
          border-radius: 2px;
        }
        .stroke-1 {
          background: var(--pink);
        }

        .stroke-2 {
          background: var(--gray);
        }

        .leg {
          display: flex;
          align-items: center;
          width: 33%;
          margin-left: 30px;
          color: var(--gray);
          font-size: 16px;
        }
      }
    }

    .chart {
      width: 686px;
      height: 355px;
      margin: 30px 10px 0px;
    }
  }

  .Box-additional-info {
    width: 342px;
    height: 545px;
    border-left: 1px solid var(--gray);

    .total-billing,
    .analyze {
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      border-bottom: 1px solid var(--gray);
      height: 20%;
      h3 {
        color: var(--gray);
        font-size: 18px;
      }
      p {
        font-weight: bold;
        font-size: 23px;
        text-transform: capitalize;
      }
    }

    .analyze {
      border: none;
      p {
        color: var(--green);
      }
    }
  }
`;
