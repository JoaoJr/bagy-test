import { ContainerStyled } from "./styles";

import CardFilterStore from "../CardFilterStore";
import CardFilterMonth from "../CardFilterMonth";
import CardFilterYear from "../CardFilterYear";

import { months, years, stores, dataUsers } from "../../Mock";
import formatValue from "../../utils/formatValue";

import { useTestData } from "../../provider/testProvider";

import {
  XAxis,
  YAxis,
  AreaChart,
  Area,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
} from "recharts";

const Chart = () => {
  const { testData } = useTestData();

  const { users } = testData[0];
  console.log(users);

  return (
    <ContainerStyled>
      <div className="Box-chart-info">
        <div className="chart-info-top">
          <div className="title">
            <h3>Total de faturamento mensal</h3>
            <span>
              {testData[0].month} {testData[0].year}
            </span>
          </div>
          <div className="legend">
            <div className="leg">
              <div className="strokes  stroke-1"></div>
              <p> Este mês</p>
            </div>
            <div className="leg">
              <div className="strokes  stroke-2"></div>
              <p>Mes passado</p>
            </div>
          </div>
        </div>
        <section className="chart">
          <ResponsiveContainer width="100%" height="100%">
            <AreaChart
              data={dataUsers}
              margin={{
                top: 50,
                right: 10,
                left: 10,
                bottom: 10,
              }}
            >
              <defs>
                <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                  <stop offset="5%" stopColor="#eee" stopOpacity={0.4} />
                  <stop offset="95%" stopColor="#37F3FF" stopOpacity={0} />
                </linearGradient>
                <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
                  <stop offset="5%" stopColor="#eee" stopOpacity={0.3} />
                  <stop offset="95%" stopColor="#37F3FF" stopOpacity={0} />
                </linearGradient>
              </defs>

              <XAxis dataKey="name" />
              <YAxis orientation="right" />
              <CartesianGrid
                strokeDasharray="3 3"
                opacity="0.1"
                stroke="#252733"
              />

              <Area
                type="monotone"
                dataKey="atual"
                stroke="#FC3C8D"
                fillOpacity={1}
                fill="url(#colorUv)"
              />
              <Area
                type="monotone"
                dataKey="passado"
                stroke="#9FA2B4"
                fillOpacity={1}
                fill="url(#colorPv)"
              />
              <Tooltip />
            </AreaChart>
          </ResponsiveContainer>
        </section>
      </div>

      <div className="Box-additional-info">
        <CardFilterStore data={stores} title={"Loja"} />
        <CardFilterMonth data={months} title={"Mês"} />
        <CardFilterYear data={years} title={"Ano"} />
        <div className="total-billing">
          <h3>Total de faturamento</h3>
          <p>{formatValue(testData[0].users[0].faturamentoTotalMes)}</p>
        </div>
        <div className="analyze">
          <h3>Análise comparativa</h3>
          <p>{testData[0].users[0].analyze}</p>
        </div>
      </div>
    </ContainerStyled>
  );
};

export default Chart;
