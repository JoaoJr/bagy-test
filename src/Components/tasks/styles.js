import styled from "styled-components";

export const ContainerStyled = styled.div`
  .Task-title {
    display: flex;
    flex-wrap: nowrap;
    justify-content: space-evenly;
    margin: 37px 0;

    h3 {
      font-family: "Mulish";
      font-size: 18px;
      width: 20%;
      margin-left: 25px;
    }
  }
`;
