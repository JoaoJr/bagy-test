import { ContainerStyled } from "./styles";
import { useFilters } from "../../provider/filtersProvider";

const CardFilterStore = ({ data, keyData, title }) => {
  const { setFilterStore } = useFilters();

  return (
    <ContainerStyled>
      <div className="info-title-store">{title}</div>
      <select onChange={(e) => setFilterStore(e.target.value)}>
        {data.map((data, index) => (
          <option key={index} value={data.store}>
            {data.store}
          </option>
        ))}
      </select>
    </ContainerStyled>
  );
};

export default CardFilterStore;
