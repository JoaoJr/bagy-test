import styled from "styled-components";

export const ContainerStyled = styled.nav`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  .title-page {
    width: 33%;
    height: 60px;
    text-align: center;
    margin-left: 20px;
    display: flex;
    align-items: center;
    h2 {
      color: var(--title);
    }
  }

  .menu-nav {
    padding-top: 20px;
    margin-right: 30px;
    width: 50%;
    display: flex;
    justify-content: flex-end;
    align-items: center;

    input {
      height: 33px;
      width: 150px;
      border: 1px solid var(--gray);
      border-radius: 15px;
    }
  }

  .img-profile {
    width: 90%;
    height: 90%;
    border-radius: 50%;
  }

  figure {
    border: 2px solid var(--gray);
    border-radius: 50%;
    width: 60px;
    height: 60px;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  h4 {
    width: 100px;
    font-size: 14px;
    margin-right: 13px;
    text-align: center;
  }

  .line-vertical {
    height: 32px;
    width: 3px;
    background: var(--gray);

    border-radius: 2px;
    margin-right: 13px;
  }
  .icon-notification {
    margin: 0px 30px 0px 23px;
    background: none;
  }

  button {
    margin: 0px 13px;
    background: none;
    border: none;
  }
`;
