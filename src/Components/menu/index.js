import { ContainerStyled } from "./styles";
import { useState } from "react";
import SearchIcon from "../../assets/img/search.svg";
import NotificationIcon from "../../assets/img/bell.svg";
import { useTestData } from "../../provider/testProvider";

const Menu = ({ title }) => {
  const { testData } = useTestData();
  const [isShown, setIsShown] = useState(false);

  const handleShow = () => {
    setIsShown(!isShown);
  };
  return (
    <ContainerStyled>
      <div className="title-page">
        <h2>{title}</h2>
      </div>
      <div className="menu-nav">
        {isShown && <input />}
        <button onClick={() => handleShow()}>
          <img src={SearchIcon} alt="SearchIcon" />
        </button>
        <img
          className="icon-notification"
          src={NotificationIcon}
          alt="NotificationIcon"
        />
        <div className="line-vertical" />
        <h4>
          {testData[0].users[0].name.length > 15
            ? `${testData[0].users[0].name.substring(0, 15)}...`
            : testData[0].users[0].name}
        </h4>
        <figure>
          <img
            className="img-profile"
            src={"https://picsum.photos/200"}
            alt="imgProfile"
          />
        </figure>
      </div>
    </ContainerStyled>
  );
};
export default Menu;
