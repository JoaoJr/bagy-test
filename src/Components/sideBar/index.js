import { useNavigate } from "react-router-dom";
import { ContainerStyled } from "./styles";
import GroupIcon from "../../assets/img/Group.svg";
import OverviewIcon from "../../assets/img/overview.svg";
import StoreIcon from "../../assets/img/tickets.svg";
import SalesIcon from "../../assets/img/ideas.svg";
import CustomersIcon from "../../assets/img/contacts.svg";
import ProductsIcon from "../../assets/img/agents.svg";
import PlansAndGoalsIcon from "../../assets/img/articles.svg";
import SettingsIcon from "../../assets/img/settings.svg";
import LogoutIcon from "../../assets/img/subscription.svg";

const SideBar = () => {
  const history = useNavigate();

  const sendTo = (path) => {
    history(path);
  };

  return (
    <>
      <ContainerStyled>
        <div className="logo-dashboard">
          <img src={GroupIcon} alt="GroupIncon" />
          <h2>
            Dashboard
            <br />
            Bagy
          </h2>
        </div>
        <div className="buttons-sideBar">
          <button onClick={() => sendTo("overview")}>
            <img src={OverviewIcon} alt="OverviewIcon" />
            <span>Visao Geral</span>
          </button>
          <button onClick={() => sendTo("store")}>
            <img src={StoreIcon} alt="StoreIcon" />
            <span>Lojas</span>
          </button>
          <button onClick={() => sendTo("sales")}>
            <img src={SalesIcon} alt="SalesIcon" />
            <span>Vendas</span>
          </button>
          <button onClick={() => sendTo("customers")}>
            <img src={CustomersIcon} alt="CustomersIcon" />
            <span>Clientes</span>
          </button>
          <button onClick={() => sendTo("products")}>
            <img src={ProductsIcon} alt="ProductsIcon" />
            <span>Produtos</span>
          </button>
          <button onClick={() => sendTo("plansAndGoals")}>
            <img src={PlansAndGoalsIcon} alt="PlansAndGoalsIcon" />
            <span>Planos e Metas</span>
          </button>
          <button onClick={() => sendTo("settings")}>
            <img src={SettingsIcon} alt="SettingsIcon" />
            <span>Configurações</span>
          </button>
          <button onClick={() => sendTo("logout")}>
            <img src={LogoutIcon} alt="LogoutIcon" />
            <span>Sair</span>
          </button>
        </div>
      </ContainerStyled>
    </>
  );
};

export default SideBar;
