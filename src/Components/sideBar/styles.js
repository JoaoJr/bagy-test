import styled from "styled-components";

export const ContainerStyled = styled.aside`
  width: 260px;
  height: 545px;
  font-family: "Mulish";

  .logo-dashboard {
    display: flex;
    color: var(--gray);
    margin-top: 30px;

    img {
      margin-right: 3px;
      color: var(--white);
      width: 50px;
    }
  }

  .buttons-sideBar {
    display: flex;
    flex-direction: column;
    margin-top: 10px;

    button {
      width: 100%;
      height: 56px;
      background-color: var(--background);
      color: var(--gray);
      display: flex;
      align-items: center;
      padding-left: 40px;
      border: none;
      font-size: 14px;

      img {
        margin-right: 24px;
        opacity: 0.4;
      }
    }

    button:hover {
      background-color: var(--sidebar-background-active);
      color: var(--white);
      border: 2px solid var(--white);
      border-left: 2px solid var(--pink);
      border-radius: 3px;

      img {
        opacity: 1;
      }
    }
  }
`;
