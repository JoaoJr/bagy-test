// import { data } from "../../Mock";
import formatValue from "../../utils/formatValue";
import { ContainerStyled } from "./styles";

import { useTestData } from "../../provider/testProvider";

const SalesReport = () => {
  const { testData } = useTestData();

  return (
    <ContainerStyled>
      <div className="Box-general-value">
        <div className="General-value">
          <h3>Total de compras</h3>
          <div>
            Valor geral:
            {formatValue(testData[0].users[0].total)}
          </div>
        </div>
        <select>
          <option>Semanal</option>
          <option>Mensal</option>
          <option>Anual</option>
        </select>
      </div>

      <ul className="ul-details">
        {testData.map((item, index) => {
          return (
            <div key={index}>
              {item.users.map((user, i) => {
                return (
                  <li key={i}>
                    <p className="p-store">{user.store}</p>
                    <p className="p-purchases">{user.purchases} compras</p>
                    <p className="p-total">{formatValue(user.total)}</p>
                  </li>
                );
              })}
            </div>
          );
        })}
      </ul>
    </ContainerStyled>
  );
};

export default SalesReport;
