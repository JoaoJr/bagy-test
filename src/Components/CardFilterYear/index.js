import { ContainerStyled } from "./styles";
import { useFilters } from "../../provider/filtersProvider";

const CardFilterYear = ({ data, title }) => {
  const { setFilterYear } = useFilters();

  return (
    <ContainerStyled>
      <div className="info-title-year">{title}</div>
      <select onChange={(e) => setFilterYear(e.target.value)}>
        {data.map((data, index) => (
          <option key={index} value={data.year}>
            {data.year}
          </option>
        ))}
      </select>
    </ContainerStyled>
  );
};

export default CardFilterYear;
