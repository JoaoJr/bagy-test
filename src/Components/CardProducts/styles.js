import styled from "styled-components";

export const ContainerStyled = styled.div`
  li {
    display: flex;
    flex-direction: column;
  }

  .Task-details-purchase {
    display: flex;
    flex-wrap: nowrap;
    align-items: center;
    justify-content: space-around;
    height: 58px;
    border-bottom: 1px solid var(--gray);

    .Task-details-product-store {
      color: var(--gray);
      font-size: 10px;
      width: 74px;
    }

    .Task-details-product-price {
      display: flex;
      align-items: center;
      justify-content: center;
      height: 24px;
      width: 74px;
      font-size: 10px;
      font-weight: bold;
      color: var(--white);
      background-color: var(--blue);
      border-radius: 8px;
    }
    .Task-details-product-purchaseData {
      display: flex;
      align-items: center;
      justify-content: center;
      height: 24px;
      width: 74px;
      font-size: 10px;
      font-weight: bold;
      color: var(--white);
      background-color: var(--yellow);
      border-radius: 8px;
    }
  }
`;
