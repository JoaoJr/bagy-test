import { ContainerStyled } from "./styles";
import formatValue from "../../utils/formatValue";

import { data } from "../../Mock";
import { useTestData } from "../../provider/testProvider";

const CardProducts = () => {
  const { testData } = useTestData();
  return (
    <ContainerStyled>
      <ul className="Task-ul-details">
        {testData.map((item, index) => {
          return (
            <li key={index}>
              {item.users.map((user, i) => {
                return (
                  <div key={i}>
                    {user.purchaseDetails.map((p, indexP) => {
                      return (
                        <div key={indexP}>
                          <div className="Task-details-purchase">
                            <div className="Task-details-product-store">
                              {p.name} #{p.id}
                            </div>
                            <div className="Task-details-product-store">
                              {user.store}
                            </div>
                            <div className="Task-details-product-price">
                              {formatValue(p.price)}
                            </div>
                            <div className="Task-details-product-purchaseData">
                              {p.purchaseData}
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                );
              })}
            </li>
          );
        })}
      </ul>
    </ContainerStyled>
  );
};

export default CardProducts;
