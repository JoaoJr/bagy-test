import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`

*{
    margin:0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
}

:root{
            --background:#363740;
            --sidebar-background-active: #463746;
            --title: #111;
            --white: #fdfdfd;
            --gray: #9FA2B4;
            --pink: #FC3C8D;
            --blue: #2F80ED;
            --yellow: #FEC400;
            --green: #219653;
            
        }

        html{ 
          background: #F5FFFA;
        }

body {

  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
    "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", 'Mulish',
    sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  margin: 0 auto;
  width: 1440px;
  height: 1243px;
  background: var(--background);

}

.App{
  display: flex;  
}

`;
